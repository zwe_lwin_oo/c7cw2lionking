//Create a table to show every day in Feb
let dateOfFirst=window.location.pathname.split("/")[2];
// let monthName=new Date(dateOfFirst).toLocaleDateString('en',{month:'long'});
let months=["January","February","March","April","May","June","July","August","September","October","November","December",];
document.getElementById('corseat').innerHTML="";
let todaysDate=new Date();
let changemonth=todaysDate.getMonth()+1;
let mon = new Date();
mon.setDate(1);
if(mon.getMonth()!=4){
    mon = new Date(mon.getTime() - 1000*60*60*24 * (mon.getDay()-1));
}
else{
    mon = new Date(mon.getTime() - 1000*60*60*24 * (mon.getDay()+6));
}

let day = mon.getDay()

//get current month start
let curmon=todaysDate.getMonth();
console.log(months[curmon]);
let curmonthName=months[curmon];
let curmonthHTML=`
<table>
<h2>${curmonthName}</h2>
</table>
`;
document.getElementById("currentmonth").innerHTML=curmonthHTML;
//get current month end

//Change to next month
document.getElementById('nextmonth').onclick=()=>{
    document.getElementById('calendar').innerHTML="";
    mon.setMonth(changemonth++);
    mon.setDate(1);
    if(mon.getMonth()!=4){
        mon = new Date(mon.getTime() - 1000*60*60*24 * (mon.getDay()-1));
    }
    else{
        mon = new Date(mon.getTime() - 1000*60*60*24 * (mon.getDay()+6));
    }
    edimonths();
    //get current month start
    let curmonthName=months[changemonth-1];
    let curmonthHTML=`
    <table>
    <h2>${curmonthName}</h2>
    </table>
    `;
    document.getElementById("currentmonth").innerHTML=curmonthHTML;
    //get current month end
    

}
//Change to prev month
document.getElementById('prevmonth').onclick=()=>{
    document.getElementById('calendar').innerHTML="";
    mon.setMonth(mon.getMonth()-1);
    mon.setDate(1);
    mon = new Date(mon.getTime() - 1000*60*60*24 * (mon.getDay()-1)); 
    edimonths();
    //get current month start
    let curmonthName=months[changemonth-1];
    let curmonthHTML=`
    <table>
    <h2>${curmonthName}</h2>
    </table>
    `;
    document.getElementById("currentmonth").innerHTML=curmonthHTML;
    //get current month end

}
//adding checkout start

//adding checkout end

function edimonths(){
    //starting fun
      
let n = mon.getTime();
for(let i=0; i<35; i++){
  const options = {
    day:'2-digit',
    month:'short',
    weekday:'short'
  };
  let dy = new Date(n + 1000*60*60*24*i);
  const d = document.createElement("div");
  let fday= dy.toISOString().substring(0, 10);
  d.id="d"+fday;
  
  d.innerHTML = dy.toLocaleDateString('en',options);
  document.getElementById("calendar").appendChild(d); 
}


for (let td of document.querySelectorAll('#calendar div')){
    let dateOfBox=new Date(td.id.substring(1));
    let dayAfter = new Date(dateOfBox.getTime()+1000*60*60*24);
    let isPastClass='';
    if (dayAfter<=todaysDate){
        isPastClass='past'
    };
    let daynum=td.innerText;
    td.innerHTML=`<div class='box ${isPastClass}'><div class=daynum>${daynum}</div><div id=availabilty></div></div>`;
}

fetch('https://tw.igs.farm/lionking/all.json')
.then(r=>r.json())
.then(r=>{
    let performances=r.data.getShow.show.performances;
    for (let p of performances){
        let pd=p.dates.performanceDate; 
        let cellId="d"+pd.split("T")[0];
        let cell=document.getElementById(cellId);
        if (cell !== null){
            let performanceDiv=document.createElement('div');
            performanceDiv.classList.add(p.performanceTimeDescription);
            let line1=document.createElement('div');
            line1.innerHTML=`<div class='dot ${p.availabilityStatus}'></div> ${pd.substr(11,5)}`;
            let line2=document.createElement('div');
            line2.classList.add('price');
            line2.innerHTML=`From £ ${p.price.minPrice}`;
            performanceDiv.append(line1,line2);
            performanceDiv.onclick = ()=>{
                document.getElementById('currentmonth').innerHTML="";
                document.getElementById('availability').innerHTML="";
                document.getElementById('monthshow').innerHTML="";
                document.getElementById('weekdays').innerHTML="";
                document.getElementById('calendar').innerHTML="";
                document.getElementById('movemonth').innerHTML="";
                if(cell.firstElementChild.classList.contains('past')){
                    alert('Not available!!!!');
                    return;
                }
                document.getElementById('calendar').classList.add(p.id);
                //console.log("I should be getting data for: ", p.id);
                fetch('https://tw.igs.farm/lionking/'+p.id)
                .then(r=>r.json())
                .then(r=>{
                    for(let s of r.seats){
                        let d = document.createElement('div');
                        d.classList.add('seat');
                        d.classList.add('Z'+s.zone);
                        d.style.left=(s.x/2+500)+'px';
                        d.style.top=(s.y/2+200)+'px';
                        if (s.available){
                            d.classList.add('available')
                        }
                        document.getElementById('seats').append(d);
                        document.getElementById('corseat').innerHTML=`

                        Seats:
            <button class="disdot available Z12B44C00-FEA3-4B1C-A903-183B4BF65BF6"></button>£23.50  &nbsp&nbsp&nbsp
            <button class="disdot available Z3D4FC0DB-E961-4ABE-93B7-74B5DB293043"></button>£41.00  &nbsp&nbsp&nbsp
            <button class="disdot available ZE9613086-CAB9-412D-BCDE-AF44F85308FE"></button>£58.50  &nbsp&nbsp&nbsp
            <button class="disdot available Z5CAA4A20-8ADF-419E-84EC-53010E022329"></button>£78.50  &nbsp&nbsp&nbsp
            <button class="disdot available Z9DCE98DD-8CED-4D28-9CFC-F56B599E3457"></button>£88.50  &nbsp&nbsp&nbsp
            <button class="disdot available Z8D781BA2-EB6F-4B14-9BE1-0CD13DE688A5"></button>£128.50  &nbsp&nbsp&nbsp
            <button class="disdot available ZC14106B5-19D0-4926-A134-14AFF3E8C6DD"></button>£138.50  &nbsp&nbsp&nbsp
            <button class="disdot available ZCE2685E0-C355-42FC-8464-F2E0BF817BF9"></button>£158.50  &nbsp&nbsp&nbsp
            <button class="disdot available ZD2B0BC25-BCFB-4603-BD00-4FE2E7E0A565"></button>£183.50
                        
                        
                        `;
                        //console.log('Z'+s.zone);
                        d.onclick = ()=>{
                            //getting the zone of seat
                            //console.log(s.zone);
                            //getting the overall zone
                            //console.log(r.zones);
                            //the same id is (s.zone) and zone.id
                            let x=s.zone;
                            let y=r.zones;
                            //console.log("The zone of seat is: ",x);
                            //console.log("The zone of r is: ",y);
                            //getting final output
                            let defticket=y[x].defaultTicket;
                            let totalprice=y[x].tickets[defticket].total;
                            console.log(totalprice);

                            alert(totalprice);

                            

                        }
                    };
                })

            }
            cell.querySelector('.box').append(performanceDiv);
        }

    }

})

document.getElementsByClassName('checkout').onclick=()=>{
    alert("Your click button work");

}

    //ending fun
}


edimonths();